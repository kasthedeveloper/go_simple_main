from http.server import BaseHTTPRequestHandler, HTTPServer
import re
import subprocess
import os, sys

# Define the path to the docker-compose file
DOCKER_COMPOSE_FILE = '/home/Go/meisam.com/Xtra/docker-compose.yml'
DOCKER_COMPOSE_DIR = '/home/Go/meisam.com/Xtra'

class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # Extract the commit short SHA from the URL
        commit_short_sha = re.search(r'/([a-fA-F0-9]+)$', self.path)
        if commit_short_sha:
            commit_short_sha = commit_short_sha.group(1)

            # Extract the route from the URL
            route = re.search(r'^/([a-z]+)/[a-fA-F0-9]+$', self.path)
            if route:
                route = route.group(1)
            else:
                self.send_response(404)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(b'Not found')
                return

            # Replace the image tag in the docker-compose file
            with open(DOCKER_COMPOSE_FILE, 'r') as f:
                content = f.read()
                if route == 'back':
                    #new_content = re.sub(r'image: 49.12.220.117:9091/ad-net-back/ad-net-back:[a-fA-F0-9]+', f'image: 49.12.220.117:9091/ad-net-back/ad-net-back:{commit_short_sha}', content)
                    new_content = re.sub(r'(image: 49.12.220.117:9091/ad-net-back/ad-net-back:)[a-fA-F0-9]+', rf'\1{commit_short_sha}', content)
                elif route == 'front':
                    #new_content = re.sub(r'image: 49.12.220.117:9091/ad-net-front/ad-net-front:[a-fA-F0-9]+', f'image: 49.12.220.117:9091/ad-net-front/ad-net-front:{commit_short_sha}', content)
                    new_content = re.sub(r'(image: 49.12.220.117:9091/ad-net-front/ad-net-front:)[a-fA-F0-9]+', rf'\1{commit_short_sha}', content)
                else:
                    self.send_response(404)
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()
                    self.wfile.write(b'Not found')
                    return

            # Write the updated content back to the docker-compose file
            with open(DOCKER_COMPOSE_FILE, 'w') as f:
                f.write(new_content)

            # Change directory and run docker-compose up -d
            os.chdir(DOCKER_COMPOSE_DIR)
            os.system("docker-compose up -d")

            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes(f'Updated docker-compose with image tag: {commit_short_sha}', 'utf-8'))
        else:
            self.send_response(404)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(b'Not found')

def run(server_class=HTTPServer, handler_class=RequestHandler, port=8082):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting server on port {port}...')
    httpd.serve_forever()

if __name__ == '__main__':
    run()